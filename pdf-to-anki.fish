#!/usr/bin/fish
set pdf (realpath $argv[1])
set name $argv[2]

mkdir $name
cd $name

pdftoppm $pdf $name

for i in (seq -w (ls *.ppm | count));
    convert $name-$i.ppm $name-(math $i - 1).png
end

rm $name-0.png *.ppm

for i in (seq 1 2 (ls *.png | count));
    printf "<img src=\"$name-$i.png\" />\t<img src=\"$name-"(math $i + 1)".png\" />\n" >> $name.tsv
end

cp *.png ~/.local/share/Anki2/'User 1/'collection.media/
