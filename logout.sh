#!/bin/sh
options="-Exit\n-Lock\n-Reboot\n-Shutdown"
useroption="$(printf $options |fzf --layout=reverse)"
set -- $(printf $options | sed "s/\n/ /g")
if [ $useroption = $1 ]; then
    swaymsg exit
elif [ $useroption = $2 ]; then
    swaylock
elif [ $useroption = $3 ]; then
    systemctl reboot
elif [ $useroption = $4 ]; then
    systemctl poweroff
fi
