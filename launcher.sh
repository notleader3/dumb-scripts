#!/usr/bin/env sh
commands=""
for i in $(echo $PATH | sed "s/:/ /g"); do
    commands="$commands$(ls $i)\n"
done
commands=$(printf "firefox\nemacs\nzoom\ndiscord\nteams\n$(printf "$commands" | sort )" | awk '!x[$0]++')

command=$(printf "$commands" | fzf --layout=reverse)

swaymsg exec $command
